const canvas = document.getElementById("canvas");
const zoom = document.getElementById("zoom");
const coords = document.getElementById("coords");
const wsClosed = document.getElementById("wsclosed");
const infoBtn = document.getElementById("infobtn");
const info = document.getElementById("info");
const infoClose = document.getElementById("infoclose");

fetch("/info.html").then(res=>res.text()).then(text=>{
    info.innerHTML = text;
    info.insertAdjacentElement("afterbegin", infoClose);
    infoClose.style.display = "block";
    infoClose.onclick = function(){
        info.style.display = '';
        coords.style.display = '';
        updateCoords();
    }
});

let socket;

function createSocket(){
    socket = new WebSocket((window.location.protocol == "http:" ? "ws://" : "wss://") + window.location.host + "/ws");
    socket.binaryType = "arraybuffer";
    socket.onmessage = handleSocketMessage;
    socket.onclose = handleSocketClose;
    socket.onopen = function(){
        wsClosed.style.display = "none";
    }
}

createSocket();

function handleSocketClose(evt){
    wsClosed.style.display = '';
    if(evt.code == 1006){
        createSocket();   
    }else{
        alert(`WebSocket closed: ${evt.code}\n\n${evt.reason}`);
    }
}

canvas.width = window.innerWidth;
canvas.height = window.innerHeight

const storedTiles = {};
const fetchingTiles = {};
let oldTileCoords = [0, 0, 0, 0];
let mouseX = 0, mouseY = 0;
let zoomTimeout = undefined;

const ctx = canvas.getContext("2d");

const maxScale = 16;
const minScale = 0.5;
let scale = Number(localStorage.getItem('z') ?? 1);
let x = Number(localStorage.getItem('x'));
let y = Number(localStorage.getItem('y'));
function handleHash(hashString){
    const parts = hashString.split(',').map(part=>Number(part));
    if(parts.length >= 3 && !parts.includes(NaN)){
        scale = Math.max(minScale, Math.min(maxScale, parts[0]));
        x = parts[1] - canvas.width / scale / 2;
        y = parts[2] - canvas.height / scale / 2;
        normalizeCoords();
        updateTiles();
        updateCoords();
        setZoomLabel();
    }
}
if(window.location.hash){
    const hashString = window.location.hash.slice(1);
    handleHash(hashString);
}
window.onhashchange = function(){
    handleHash(window.location.hash.slice(1));
}
let button;
let moving = false;

function updateCoords(){
    const pointerX = x + mouseX / scale;
    const pointerY = y + mouseY / scale;
    coords.textContent = `${Math.floor(pointerX)},${Math.floor(pointerY)}`;
    coords.style.left = `${mouseX}px`;
    coords.style.top = `${mouseY - coords.clientHeight}px`;
}

function bufferToImage(buffer){
    const buf = new Uint8Array(buffer);
    const array = new Uint8ClampedArray(256 * 256 * 4);
    for(let i = 0; i < 256 * 256; i++){
        array.set(buf.slice(i * 3, i * 3 + 3), i * 4);
        array[i * 4 + 3] = 255;
    }
    return new ImageData(array, 256, 256);
}

function drawTile(tileX, tileY){
    const tile = storedTiles[`${tileX}/${tileY}`];
    if(!tile) return;
    if(scale > 1){
        ctx.imageSmoothingEnabled = false;
    }else{
        ctx.imageSmoothingEnabled = true;
    }
    ctx.drawImage(
        tile.canvas,
        Math.floor(scale * (tileX * 256 - x)),
        Math.floor(scale * (tileY * 256 - y)),
        Math.ceil(256 * scale),
        Math.ceil(256 * scale)
    );
}

let queuedRedraw = false;
function redrawTiles(){
    if(queuedRedraw) return;
    queuedRedraw = true;
    requestAnimationFrame(()=>{
        queuedRedraw = false;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        for(let tile in storedTiles){
            const [tileX, tileY] = tile.split('/');
            drawTile(tileX, tileY);
        }
    })
}

async function fetchTile(tileX, tileY, tilePath){
    fetchingTiles[tilePath] = true;
    const res = await fetch("/tile/" + tilePath).catch(console.error);
    if(!res){
        delete fetchingTiles[tilePath];
        return;
    }
    const buf = await res.arrayBuffer().catch(console.error);
    delete fetchingTiles[tilePath];
    if(!buf) return;
    const tileCanvas = new OffscreenCanvas(256, 256);
    const imageData = bufferToImage(buf);
    const tileCtx = tileCanvas.getContext("2d");
    tileCtx.putImageData(imageData, 0, 0);
    storedTiles[tilePath] = {canvas: tileCanvas, ctx: tileCtx, data: imageData};
    drawTile(tileX, tileY);
}

function getTiles(tiles){
    for(let tile of tiles){
        const tilePath = `${tile[0]}/${tile[1]}`;
        if(storedTiles[tilePath] != undefined) continue;
        if(fetchingTiles[tilePath] != undefined) continue;
        fetchTile(tile[0], tile[1], tilePath);
    }
}

function purgeTiles(x1tile, x2tile, y1tile, y2tile){
    for(let tile in storedTiles){
        const [tileX, tileY] = tile.split('/');
        if(tileX < x1tile || tileX > x2tile || tileY < y1tile || tileY > y2tile) delete storedTiles[tile];
    }
}

function updateTilePos(tileCoords){
    let equal = true;
    for(let i = 0; i < 4; i++){
        if(tileCoords[i] != oldTileCoords[i]) equal = false;
    }
    oldTileCoords = tileCoords;
    if(!equal && socket.readyState == 1) socket.send(new Uint8Array(tileCoords));
}

function updateTiles(){
    const x1 = x;
    const y1 = y;
    const x2 = x1 + canvas.width / scale;
    const y2 = y1 + canvas.height / scale;
    const x1tile = Math.max(0, Math.floor(x1 / 256) - 1);
    const x2tile = Math.min(255, Math.floor(x2 / 256) + 1);
    const y1tile = Math.max(0, Math.floor(y1 / 256) - 1);
    const y2tile = Math.min(255, Math.floor(y2 / 256) + 1);
    purgeTiles(x1tile, x2tile, y1tile, y2tile);
    const tiles = [];
    for(let ytile = y1tile; ytile <= y2tile; ytile++){
        for(let xtile = x1tile; xtile <= x2tile; xtile++){
            if(xtile < 0 || xtile > 255) continue;
            if(ytile < 0 || ytile > 255) continue;
            tiles.push([xtile, ytile]);
        }
    }
    updateTilePos([x1tile, x2tile, y1tile, y2tile]);
    getTiles(tiles);
}

window.onresize = function(){
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight
    ctx.imageSmoothingEnabled = false;
    updateTiles();
    redrawTiles();
}

updateTiles();
updateCoords();

document.oncontextmenu = function(){
    return false;
}

function normalizeCoords(){
    if(x < 0) x = 0;
    if(x + canvas.width / scale > 65536) x = 65536 - canvas.width / scale;
    if(y < 0) y = 0;
    if(y + canvas.height / scale > 65536) y = 65536 - canvas.height / scale;
}

let xMovePrev, yMovePrev;
function translate(dX, dY){
    x -= dX / scale;
    y -= dY / scale;
    normalizeCoords();
    updateTiles();
    redrawTiles();
    updateCoords();
}

document.onpointerdown = function(evt){
    if(info.style.display == 'block') return;
    if(moving) return;
    button = evt.button
    moving = true;
    xMovePrev = evt.clientX;
    yMovePrev = evt.clientY;
}

function setWindowhash(){
    window.location.hash = `${scale.toFixed(2)},${Math.round(x + canvas.width / scale / 2)},${Math.round(y + canvas.height / scale / 2)}`;
}

document.onpointerup = function(evt){
    if(!moving) return;
    if(evt.button != button) return;
    moving = false;
    translate(evt.clientX - xMovePrev, evt.clientY - yMovePrev);
    localStorage.setItem('x', x);
    localStorage.setItem('y', y);
    setWindowhash();
}

let infoTimeout = undefined;
infoBtn.onclick = function(){
    if(infoTimeout != undefined) clearTimeout(infoTimeout);
    infoBtn.style.display = 'none';
    info.style.display = "block";
    coords.style.display = "none";
}

document.onpointermove = function(evt){
    mouseX = evt.clientX;
    mouseY = evt.clientY;
    if(info.style.display == "block") return;
    infoBtn.style.display = "block";
    if(infoTimeout != undefined) clearTimeout(infoTimeout);
    infoTimeout = setTimeout(()=>{
        infoBtn.style.display = 'none';
        infoTimeout = undefined;
    }, 2000);
    if(!moving) return updateCoords();
    translate(evt.clientX - xMovePrev, evt.clientY - yMovePrev);
    xMovePrev = evt.clientX;
    yMovePrev = evt.clientY;
    updateCoords();
}

function hideZoom(){
    zoomTimeout = undefined;
    zoom.style.display = '';
}

function setZoomLabel(){
    if(zoomTimeout != undefined) clearTimeout(zoomTimeout);
    zoom.textContent = `${scale.toFixed(2)}x`;
    zoom.style.display = "block";
    zoomTimeout = setTimeout(hideZoom, 2000);
}

document.onwheel = function(evt){
    if(info.style.display == 'block') return;
    const zoomMultiplier = 1 + -evt.deltaY / 1000;
    let newScale = Math.min(maxScale, Math.max(minScale, scale * zoomMultiplier));
    const cursorX = evt.clientX / scale + x;
    const cursorY = evt.clientY / scale + y;
    const newCursorX = evt.clientX / newScale + x;
    const newCursorY = evt.clientY / newScale + y;
    x += cursorX - newCursorX;
    y += cursorY - newCursorY;
    normalizeCoords()
    scale = newScale;
    setZoomLabel();
    updateTiles();
    redrawTiles();
    updateCoords();
    localStorage.setItem('z', newScale);
    setWindowhash();
}

document.body.onpointerleave = function(){
    coords.style.display = "none";
}

document.body.onpointerenter = function(){
    coords.style.display = '';
}

let drawQueue = {};

let enqueuedDraw = false;
function processDrawQueue(){
    for(tileName in drawQueue){
        const tile = storedTiles[tileName];
        if(!tile) continue;
        const queuedTile = drawQueue[tileName];

        for(let drawCommand of queuedTile){
            const xPixel = drawCommand[1];
            const yPixel = drawCommand[3];
            tile.data.data.set(drawCommand.slice(4), (yPixel * 256 + xPixel) * 4);
        }

        const tileX = queuedTile[0][0];
        const tileY = queuedTile[0][2];
        tile.ctx.putImageData(tile.data, 0, 0);
        drawTile(tileX, tileY);
    }
    drawQueue = {};
    enqueuedDraw = false;
}

function handleSocketMessage(msg){
    const array = new Uint8Array(msg.data);
    for(let i = 0; i < array.length; i += 7){
        const tileX = array[i + 0];
        const tileY = array[i + 2];

        const tileName = `${tileX}/${tileY}`;
        if(drawQueue[tileName] == undefined) drawQueue[tileName] = [];
        drawQueue[tileName].push(array.slice(i, i + 7));
        if(!enqueuedDraw){
            requestAnimationFrame(processDrawQueue);
            enqueuedDraw = true;
        }
    }
}
