/*
    IPv6 Canvas
    Copyright (C) 2024 Zipdox

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <sys/mman.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <libsoup/soup.h>
#include <glib-unix.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "blocklist.h"

#define SERVER_VERSION "0.1"

#define SOCK_RX_BUFSZ (32 * 1024 * 1024)

// NOTE: for multithreading, you need separate copies of these per thread
// could use TLS or, if max number of threads is known, an array of a struct
// containing all of these, where the array index is the thread index
#define RECV_BATCH_SIZE (1024ul)

static unsigned char       icmp_type[RECV_BATCH_SIZE];
static struct iovec        iov[RECV_BATCH_SIZE];
static struct sockaddr_in6 srcaddr[RECV_BATCH_SIZE];
static unsigned char       control_buf[RECV_BATCH_SIZE * CMSG_SPACE(sizeof(struct in6_pktinfo))];
static struct mmsghdr      msgs[RECV_BATCH_SIZE];

off_t file_size = ((long)1 << 16) * ((long)1 << 16) * 3;
gsize tile_size = 256 * 256 * 3;
gsize row_size = 256 * 256 * 3 * 256;

typedef struct {
    unsigned char *image;
    GList *users;
    GMainLoop *loop;
    gboolean run;
    Blocklist *blocklist;
    char *info_html;
    gsize info_html_len;
} Server;

typedef struct {
    long x1, x2;
    long y1, y2;
    SoupWebsocketConnection *connection;
    GByteArray *ws_buffer;
    Server *server;
} User;

gboolean handle_int(gpointer user_data){
    g_main_loop_quit(user_data);
    return G_SOURCE_REMOVE;
}

gboolean ignore_signal(gpointer user_data){
    return G_SOURCE_CONTINUE;
}

void load_info_html(Server *server, const char *path){
    char *old_info = server->info_html;
    GFile *file = g_file_new_for_path(path);
    GError *error = NULL;
    gboolean file_loaded = g_file_load_contents(file, NULL, &server->info_html, &server->info_html_len, NULL, &error);
    g_object_unref(file);
    if(!file_loaded){
        server->info_html = NULL;
        fprintf(stderr, "Failed to load info: %s\n", error->message);
        g_error_free(error);
    }
    g_free(old_info);
}

void load_blocklist(Server *server, const char *path){
    Blocklist *old_bl = server->blocklist;
    server->blocklist = Blocklist_create(path);
    Blocklist_destroy(old_bl);
}

gboolean handle_usr1(gpointer user_data){
    Server *server = user_data;

    load_info_html(server, "info.html");

    load_blocklist(server, "blocklist.txt");

    return G_SOURCE_CONTINUE;
}

void socket_closed(SoupWebsocketConnection *self, gpointer user_data){
    User *user = user_data;
    user->server->users = g_list_remove(user->server->users, user);
    g_object_unref(self);
    g_byte_array_free(user->ws_buffer, TRUE);
    if(!user->server->run && user->server->users == NULL) g_main_loop_quit(user->server->loop);
    g_free(user);
}

gboolean send_user_buffers(gpointer user_data){
    Server *server = user_data;
    GList *user_l = server->users;
    while(user_l){
        User *user = user_l->data;
        SoupWebsocketState ws_state = soup_websocket_connection_get_state(user->connection);
        if(ws_state == SOUP_WEBSOCKET_STATE_CLOSING){
            user_l = user_l->next;
            continue;
        }
        if(ws_state == SOUP_WEBSOCKET_STATE_CLOSED){ // No idea why this happens, socket_closed should've been called
            user_l = user_l->next;
            socket_closed(user->connection, server);
            continue;
        }
        if(user->ws_buffer->len > 0){
            soup_websocket_connection_send_binary(user->connection, user->ws_buffer->data, user->ws_buffer->len);
            g_byte_array_set_size(user->ws_buffer, 0);
        }
        user_l = user_l->next;
    }

    return G_SOURCE_CONTINUE;
}

void update_users(Server *server, const unsigned char coords[4], const unsigned char pixels[3]){
    const unsigned char data[7] = {coords[0], coords[1], coords[2], coords[3], pixels[0], pixels[1], pixels[2]};

    GList *user_l = server->users;
    while(user_l){
        User *user = user_l->data;

        if(user->x1 <= coords[0] && user->x2 >= coords[0] && user->y1 <= coords[2] && user->y2 >= coords[2])
            g_byte_array_append(user->ws_buffer, data, 7);
        user_l = user_l->next;
    }
}

void composite_pixel(Server *server, const unsigned char coords[4], const unsigned char new_colors[4]){
    unsigned char *image = server->image;

    long tile_x = coords[0];
    long x = coords[1];
    long tile_y = coords[2];
    long y = coords[3];

    unsigned char *tile = image + tile_y * row_size + tile_x * tile_size;
    unsigned char *pixel = tile + 3 * (y * 256 + x);

    if(new_colors[3] == 255){
        memcpy(pixel, new_colors, 3);
    }else{
        float r = new_colors[0] / 255.0f;
        float g = new_colors[1] / 255.0f;
        float b = new_colors[2] / 255.0f;
        float a = new_colors[3] / 255.0f;

        float r_old = pixel[0] / 255.0f;
        float g_old = pixel[1] / 255.0f;
        float b_old = pixel[2] / 255.0f;

        float r_new = a * r + (1.0f - a) * r_old;
        float g_new = a * g + (1.0f - a) * g_old;
        float b_new = a * b + (1.0f - a) * b_old;

        pixel[0] = r_new * 255.0f;
        pixel[1] = g_new * 255.0f;
        pixel[2] = b_new * 255.0f;
    }

    update_users(server, coords, pixel);
}

static void init_recv_buffers(void){
    for(size_t ii = 0; ii < RECV_BATCH_SIZE; ii++){
        iov[ii].iov_base = &icmp_type[ii];
        iov[ii].iov_len  = 1;

        msgs[ii].msg_hdr.msg_iov        = &iov[ii];
        msgs[ii].msg_hdr.msg_iovlen     = 1;
        msgs[ii].msg_hdr.msg_name       = &srcaddr[ii];
        msgs[ii].msg_hdr.msg_namelen    = sizeof(srcaddr[ii]);
        msgs[ii].msg_hdr.msg_control    = &control_buf[ii * CMSG_SPACE(sizeof(struct in6_pktinfo))];
        msgs[ii].msg_hdr.msg_controllen = CMSG_SPACE(sizeof(struct in6_pktinfo));
    }
}

gboolean ping_cb(guint fd, GIOCondition condition, gpointer user_data){
    Server *server = user_data;

    while(1){
        int msg_count = recvmmsg(fd, msgs, RECV_BATCH_SIZE, MSG_DONTWAIT, NULL);
        if(msg_count <= 0) return G_SOURCE_CONTINUE;

        for(int ii = 0; ii < msg_count; ii++){
            if(msgs[ii].msg_len != 1 || icmp_type[ii] != 0x80) continue;
            if(Blocklist_blocked(server->blocklist, &srcaddr[ii].sin6_addr)) continue;

            struct cmsghdr     *cmsg     = CMSG_FIRSTHDR(&msgs[ii].msg_hdr);
            struct in6_pktinfo *pkt_info = (struct in6_pktinfo*) CMSG_DATA(cmsg);
            unsigned char      *addr     = pkt_info->ipi6_addr.s6_addr;

            if(addr[15] == 0) continue; // Ignore transparent pixels

            const unsigned char *coords     = &addr[8];
            const unsigned char *new_colors = &addr[12];

            composite_pixel(server, coords, new_colors);
        }
    }

    return G_SOURCE_CONTINUE;
}

void socket_message(SoupWebsocketConnection *self, int type, GBytes *message, gpointer user_data){
    User *user = user_data;
    if(!user->server->run) return;

    gsize size = 0;
    const unsigned char *data = g_bytes_get_data(message, &size);
    if(!data) return;
    if(size < 4) return;

    user->x1 = data[0];
    user->x2 = data[1];
    user->y1 = data[2];
    user->y2 = data[3];
}

void socket_cb(SoupServer *soup_server, SoupWebsocketConnection *connection, const char *path, SoupClientContext *client, gpointer user_data){
    Server *server = user_data;
    if(!server->run) return;
    User *user = g_new0(User, 1);
    user->server = server;
    user->ws_buffer = g_byte_array_new();
    user->connection = g_object_ref(connection);
    g_signal_connect(connection, "closed", G_CALLBACK(socket_closed), user);
    g_signal_connect(connection, "message", G_CALLBACK(socket_message), user);
    server->users = g_list_append(server->users, user);
}

const char *get_mime(const char *path){
    if(g_str_has_suffix(path, ".html")) return "text/html";
    if(g_str_has_suffix(path, ".css")) return "text/css";
    if(g_str_has_suffix(path, ".js")) return "text/javascript";
    return "application/octet-stream";
}

void public_cb(SoupServer *soup_server, SoupMessage *msg, const char *path, GHashTable *query, SoupClientContext *client, gpointer user_data){
    char *real_path = (char*) path;
    if(g_strcmp0(path, "/") == 0) real_path = "/index.html";
    GBytes *resource = g_resources_lookup_data(real_path, G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
    if(!resource) return soup_message_set_status(msg, SOUP_STATUS_NOT_FOUND);
    soup_message_set_status(msg, SOUP_STATUS_OK);
    gsize size = 0;
    gconstpointer data = g_bytes_get_data(resource, &size);
    soup_message_set_response(msg, get_mime(real_path), SOUP_MEMORY_COPY, (const char*) data, size);
    g_bytes_unref(resource);
}

void tile_cb(SoupServer *soup_server, SoupMessage *msg, const char *path, GHashTable *query, SoupClientContext *client, gpointer user_data){
    Server *server = user_data;
    if(!server->run) return soup_message_set_status(msg, SOUP_STATUS_SERVICE_UNAVAILABLE);
    gchar **path_parts = g_strsplit(path, "/", 0);
    int i = 2;
    long x = -1, y = -1;
    while(path_parts[i] != NULL && i <= 3){
        if(i == 2) x = strtol(path_parts[i], NULL, 10);
        if(i == 3) y = strtol(path_parts[i], NULL, 10);
        i++;
    }
    g_strfreev(path_parts);
    if(x < 0 || y < 0) return soup_message_set_status(msg, SOUP_STATUS_BAD_REQUEST);
    if(x > 255 || y > 255) return soup_message_set_status(msg, SOUP_STATUS_NOT_FOUND);
    soup_message_set_status(msg, SOUP_STATUS_OK);
    SoupMessageHeaders *headers;
    g_object_get(msg, "response-headers", &headers, NULL);
    soup_message_headers_append(headers, "Cache-Control", "no-store");
    soup_message_set_response(msg, "application/octet-stream", SOUP_MEMORY_STATIC, (const char*) server->image + row_size * y + tile_size * x, tile_size);
}

void info_html_cb(SoupServer *soup_server, SoupMessage *msg, const char *path, GHashTable *query, SoupClientContext *client, gpointer user_data){
    Server *server = user_data;
    if(!server->info_html) return soup_message_set_status(msg, SOUP_STATUS_NOT_FOUND);
    soup_message_set_status(msg, SOUP_STATUS_OK);
    SoupMessageHeaders *headers;
    g_object_get(msg, "response-headers", &headers, NULL);
    soup_message_headers_append(headers, "Cache-Control", "no-store");
    soup_message_set_response(msg, "text/html", SOUP_MEMORY_COPY, (const char*) server->info_html, server->info_html_len);
}

int main(int argc, char *argv[]){
    int fd = open("image", O_RDWR | O_CREAT, 0600);
    if(fd == -1){
        perror("Error creating file");
        return EXIT_FAILURE;
    }
    if(ftruncate(fd, file_size) != 0){
        perror("Error setting file size");
        close(fd);
        return EXIT_FAILURE;
    }

    unsigned char *image = mmap(NULL, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(image == MAP_FAILED){
        perror("Error mapping file into memory\n");
        close(fd);
        return EXIT_FAILURE;
    }

    if(argc >= 2) if(g_strcmp0(argv[1], "check") == 0){
        printf("Starting checkerboard\n");
        int checkerboard = 0;
        for(int y = 0; y < 256; y++){
            for(int x = 0; x < 256; x++){
                unsigned char *tile = image + row_size * y + tile_size * x;
                memset(tile, checkerboard * 255, tile_size);
                checkerboard = !checkerboard;
            }
            checkerboard = !checkerboard;
        }
        printf("Done checkerboarding\n");

        munmap(image, file_size);
        close(fd);
        return EXIT_SUCCESS;
    }

    if(argc < 3){
        fprintf(stderr, "Usage: ipv6-canvas [address] [port]\n");
        munmap(image, file_size);
        close(fd);
        return EXIT_FAILURE;
    }

    int sock_fd =  socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);
    if(sock_fd < 0) {
        perror("Ping socket creation failed");
        munmap(image, file_size);
        close(fd);
        exit(EXIT_FAILURE);
    }
    int sock_flags = fcntl(sock_fd, F_GETFL, 0);
    if(sock_flags == -1){
        perror("Failed to get socket flags");
        munmap(image, file_size);
        close(fd);
        close(sock_fd);
        exit(EXIT_FAILURE);
    }
    sock_flags |= O_NONBLOCK;
    if(fcntl(sock_fd, F_SETFL, sock_flags) == -1){
        perror("Failed to set socket flags");
        munmap(image, file_size);
        close(fd);
        close(sock_fd);
        exit(EXIT_FAILURE);
    }

    int on = 1;
    if(setsockopt(sock_fd, IPPROTO_IPV6, IPV6_RECVPKTINFO, &on, sizeof(on)) == -1){
        perror("Ping socket creation failed");
        munmap(image, file_size);
        close(fd);
        close(sock_fd);
        exit(EXIT_FAILURE);
    }
    if(setsockopt(sock_fd, SOL_SOCKET, SO_RCVBUFFORCE, &(int){SOCK_RX_BUFSZ}, sizeof(int)) == -1){
        perror("Failed to set receive buffer size");
        munmap(image, file_size);
        close(fd);
        close(sock_fd);
        exit(EXIT_FAILURE);
    }

    init_recv_buffers();

    GMainLoop *loop = g_main_loop_new(NULL, FALSE);

    SoupServer *soup_server = soup_server_new("server-header", "IPv6 Canvas/" SERVER_VERSION, NULL);

    Server server = {
        image,
        NULL,
        loop,
        TRUE,
        NULL,
        NULL,
        0
    };

    load_blocklist(&server, "blocklist.txt");
    load_info_html(&server, "info.html");

    GSource *ping_source = g_unix_fd_source_new(sock_fd, G_IO_IN);
    g_source_attach(ping_source, NULL);
    g_source_set_callback(ping_source, G_SOURCE_FUNC(ping_cb), &server, NULL);

    soup_server_add_websocket_handler(soup_server, "/ws", NULL, NULL, socket_cb, &server, NULL);
    soup_server_add_handler(soup_server, "/tile/", tile_cb, &server, NULL);
    soup_server_add_handler(soup_server, "/info.html", info_html_cb, &server, NULL);
    soup_server_add_handler(soup_server, "/", public_cb, NULL, NULL);

    unsigned long port = strtoul(argv[2], NULL, 10);
    if(port > UINT_MAX) port = UINT_MAX;
    GSocketAddress *address = g_inet_socket_address_new_from_string(argv[1], port);
    if(!address){
        fprintf(stderr, "Error: Invalid address or port\n");
        munmap(image, file_size);
        close(fd);
        close(sock_fd);
        return EXIT_FAILURE;
    }

    GError *error = NULL;
    if(!soup_server_listen(soup_server, address, 0, &error)){
        fprintf(stderr, "Error starting server: %s\n", error->message);
        munmap(image, file_size);
        close(fd);
        close(sock_fd);
        return EXIT_FAILURE;
    }else{
        GInetAddress *inet_addr = g_inet_socket_address_get_address(G_INET_SOCKET_ADDRESS(address));
        char *add_str = g_inet_address_to_string(inet_addr);
        if(g_inet_address_get_family(inet_addr) == G_SOCKET_FAMILY_IPV6){
            printf("Listening on [%s]:%u\n", add_str, (guint)g_inet_socket_address_get_port(G_INET_SOCKET_ADDRESS(address)));
        }else{
            printf("Listening on %s:%u\n", add_str, (guint)g_inet_socket_address_get_port(G_INET_SOCKET_ADDRESS(address)));
        }
        g_free(add_str);
        g_object_unref(address);
    }

    g_unix_signal_add(SIGINT, handle_int, loop);
    g_unix_signal_add(SIGTERM, handle_int, loop);
    g_unix_signal_add(SIGHUP, ignore_signal, loop);
    g_unix_signal_add(SIGUSR1, handle_usr1, &server);

    guint update_user_timeout = g_timeout_add(500, send_user_buffers, &server);

    g_main_loop_run(loop);
    server.run = FALSE;
    g_source_remove(update_user_timeout);

    g_source_destroy(ping_source);
    g_source_unref(ping_source);

    soup_server_remove_handler(soup_server, "/ws");
    soup_server_remove_handler(soup_server, "/tile/");
    soup_server_remove_handler(soup_server, "/info.html");
    soup_server_remove_handler(soup_server, "/");

    if(server.users != NULL){
        GList *user_l = server.users;
        while(user_l){
            User *user = user_l->data;
            soup_websocket_connection_close(user->connection, SOUP_WEBSOCKET_CLOSE_GOING_AWAY, NULL);
            user_l = user_l->next;
        }
        g_main_loop_run(loop);
    }

    g_object_unref(soup_server);

    Blocklist_destroy(server.blocklist);
    g_free(server.info_html);
    if(msync(image, file_size, MS_SYNC) == -1)
        perror("Error syncing file");
    if(munmap(image, file_size) == -1)
        perror("Error unmapping file");
    close(fd);
    close(sock_fd);

    printf("Synced and closed canvas file\n");

    return EXIT_SUCCESS;
}
