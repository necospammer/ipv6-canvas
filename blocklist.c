#include <gio/gio.h>
#include <libcidr.h>
#include <stdio.h>

#include "blocklist.h"

typedef struct {
    struct in6_addr addr;
    struct in6_addr mask;
} BlockRule;

struct _Blocklist{
    BlockRule **blocklist;
    gsize blocklist_len;
};

static void create_ipv6_netmask(struct in6_addr *netmask, int mask){
	uint32_t *p_netmask;
	memset(netmask, 0, sizeof(struct in6_addr));
	if(mask < 0){
		mask = 0;
	}else if(128 < mask){
		mask = 128;
	}

	p_netmask = &netmask->s6_addr32[0];

	while(32 < mask){
		*p_netmask = 0xffffffff;
		p_netmask++;
		mask -= 32;
	}
	if(mask != 0){
		*p_netmask = htonl(0xFFFFFFFF << (32 - mask));
	}
}

static int compare_ipv6_with_mask(
	const struct in6_addr *addr1,
	const struct in6_addr *addr2,
	const struct in6_addr *mask)
{
	struct in6_addr masked = *addr2;

	masked.s6_addr32[0] &= mask->s6_addr32[0];
	masked.s6_addr32[1] &= mask->s6_addr32[1];
	masked.s6_addr32[2] &= mask->s6_addr32[2];
	masked.s6_addr32[3] &= mask->s6_addr32[3];

	return memcmp(addr1, &masked, sizeof(struct in6_addr));
}

static BlockRule *BlockRule_create(const char *addr_str){
    CIDR *addr =  cidr_from_str(addr_str);
    BlockRule *rule = g_new0(BlockRule, 1);

    cidr_to_in6addr(addr, &rule->addr);
    int mask = cidr_get_pflen(addr);
    cidr_free(addr);
    create_ipv6_netmask(&rule->mask, mask);
    printf("\n");

    return rule;
}

Blocklist *Blocklist_create(const char *path){
    GFile *file = g_file_new_for_path(path);
    char *file_content;
    GError *error = NULL;
    gboolean file_loaded = g_file_load_contents(file, NULL, &file_content, NULL, NULL, &error);
    g_object_unref(file);
    if(!file_loaded){
        fprintf(stderr, "Failed to load blocklist: %s\n", error->message);
        g_error_free(error);
        return NULL;
    }

    g_strchomp(file_content);
    char **split = g_strsplit(file_content, "\n", 0);
    g_free(file_content);

    Blocklist *blocklist = g_new0(Blocklist, 1);

    while(split[blocklist->blocklist_len] != NULL) blocklist->blocklist_len++;
    blocklist->blocklist = g_new0(BlockRule*, blocklist->blocklist_len);

    for(gsize i = 0; i < blocklist->blocklist_len; i++){
        blocklist->blocklist[i] = BlockRule_create(split[i]);
    }    

    g_strfreev(split);

    return blocklist;
}

int Blocklist_blocked(Blocklist *blocklist, struct in6_addr *addr){
    if(!blocklist) return 0;

    for(gsize i = 0; i < blocklist->blocklist_len; i++){
        if(compare_ipv6_with_mask(&blocklist->blocklist[i]->addr, addr, &blocklist->blocklist[i]->mask) == 0) return 1;
    }

    return 0;
}

void Blocklist_destroy(Blocklist *blocklist){
    if(!blocklist) return;
    for(gsize i = 0; i < blocklist->blocklist_len; i++){
        g_free(blocklist->blocklist[i]);
    }
    g_free(blocklist->blocklist);
    g_free(blocklist);
}
