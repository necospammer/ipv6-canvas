#include <netinet/in.h>

typedef struct _Blocklist Blocklist;

Blocklist *Blocklist_create(const char *path);

int Blocklist_blocked(Blocklist *blocklist, struct in6_addr *addr);

void Blocklist_destroy(Blocklist *blocklist);
