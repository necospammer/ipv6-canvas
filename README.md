# IPv6 Canvas
Canvas web app that allows pinging an IPv6 address to draw a pixel.

Utilizes the full private space of IPv6 to create a 65536x65536 24-bit RGB canvas with alpha compositing. Consequently it takes up 12.9GB of disk space.

# Building
You just need gcc, make, and  libsoup-gnome2.4-dev. Run `make`.

# Usage
You will need to have a whole /64 IPv6 block assigned to a network interface. [Here](https://serverfault.com/a/1128204) is a serverfault thread on how to do this.

If you want to checkerboard the canvas, run `./ipv6-canvas check`.

To run the web app, run `sudo ./ipv6-canvas [address] [port]`. The address is only for the HTTP/WS server, it can be either IPv4 or IPv6. Pings are received on all IPv6 interfaces.

If you want to install it as a service, run `sudo make install`. It will create a service called `ipv6-canvas.service` (which you need to enable/start yourself) and run on port 3000 listening on 127.0.0.1. You will need a reverse proxy like NGINX, which you should also configure with compression since libsoup doesn't have it.

# Notes on VPSs
If you're looking to host this on a VPS, you need a host that sends all packets to your /64 block to your VPS. Hetzner (what I used) and Linode (requires [different configuration](https://blog.tugzrida.xyz/2023/02/06/introducing-the-ipv6-canvas/#accepting-packets)) do this. Vultr sends NDP broadcasts for every address, so it is not suitable for this project. OVH only gives out /128 to VPSs.

# License
This project is licensed under the AGPL-3.0.
